# FrontendTrades

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.1.

## How to Run the First Time
- In the `frontend-trades` folder type the command `npm install -g @angular/cli` to install Angular
- Run the main method in TradesRestApiApplication.java in the Spring-Boot-Rest-Hackathon project for the backend
- Run the main method in TradeSimulatorApplication.java in the dummy-trade-filler project for updating trade states
- Run `ng serve` or `ng serve --open` to automatically open the browser
- If that does not work type `npm install` and retry the previous step

## How to Run After First Time
- If not already, run the main method in TradesRestApiApplication.java in the Spring-Boot-Rest-Hackathon project for the backend
- If not already, run the main method in TradeSimulatorApplication.java in the dummy-trade-filler project for updating trade states
- Run `ng serve` or `ng serve --open` to automatically open the browser

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
