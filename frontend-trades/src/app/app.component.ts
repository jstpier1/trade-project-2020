import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm, FormsModule } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';

import { Trade } from './trade.model';
import { Stock } from './stock.model';
import { TradesService } from './trades.service';
import { debounce, debounceTime } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedTrades: Trade[] = [];
  loadedStocks: Stock[] = [];
  isFetching = false;
  userCashAmount: string;
  timeIntervalSeconds = 5;
  error = null;
  livePriceMap = new Map();
  private errorSub: Subscription;
  private _success = new Subject<string>();
  title = 'frontend-trades';
  successTrade : string = "";

  constructor(private http: HttpClient, private tradesService: TradesService) {
    setInterval(()=> { this.onFetchStocks() }, this.timeIntervalSeconds * 1000);
    setInterval(()=> { this.onFetchTrades() }, this.timeIntervalSeconds * 1000);
    setInterval(()=> { this.onGetUserCashAmount() }, this.timeIntervalSeconds * 1000);
    setInterval(()=> { this.onFetchLivePrices() }, this.timeIntervalSeconds * 1000);
  }

  ngOnInit() {
    this.errorSub = this.tradesService.error.subscribe(errorMessage => {
      this.error = errorMessage;
    });

    this.onGetUserCashAmount();
    this._success.subscribe(message => this.successTrade = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successTrade = '');

    this.isFetching = true;
    this.tradesService.fetchTrades().subscribe(
      trades => {
        this.isFetching = false;
        this.loadedTrades = trades;
      },
      error => {
        this.error = error.message;
      }
    );
    this.isFetching = true;
    this.tradesService.fetchStocks().subscribe(
      stocks => {
        this.isFetching = false;
        this.loadedStocks = stocks;
      },
      error => {
        this.error = error.message;
        console.log(error);
      }
    );

  }

  async onCreateTrade(tradeData : Trade) {

    if(!tradeData.ticker) {
      alert('👿 👿 👿 Please enter a ticker. 👿 👿 👿');
      return;
    }

    if(!tradeData.quantity) {
      alert('👿 👿 👿 Please enter a quantity. 👿 👿 👿');
      return;
    }

    if(!tradeData.requestedPrice) {
      alert('👿 👿 👿 Please enter a price. 👿 👿 👿');
      return;
    }

     // Validate ticker
    var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
    const getPriceUrl = priceServiceUrl + '?ticker=' + tradeData.ticker + '&num_days=1';
    const response = await fetch(getPriceUrl);
    const json = await response.json()
    if(!json['price_data']) {
      alert('👿 👿 👿 Invalid ticker! 👿 👿 👿');
      return;
    }

    // Validate quantity
    if(tradeData.quantity < 1) {
      alert('👿 👿 👿 Invalid quantity! 👿 👿 👿');
      return;
    }

    // Validate price
    if(tradeData.requestedPrice < 0.01) {
      alert('👿 👿 👿 Invalid price! 👿 👿 👿');
      return;
    }

    const lastPrice = json['price_data'][0][1].toFixed(2);
    if(tradeData.requestedPrice > lastPrice * 1.2 || tradeData.requestedPrice < lastPrice * 0.8) {
      if (!confirm("WARNING: Your requested price: $ " + tradeData.requestedPrice + " is 20% higher or lower than the current price: $ " + lastPrice + " . Would you like to continue?")) {
        return;
      }
    }

    // Send Http request
    this.tradesService.createAndStoreTrade(tradeData.ticker, tradeData.quantity, tradeData.requestedPrice, tradeData.type);
    this._success.next(`Trade for ${tradeData.quantity} of ${tradeData.ticker} at ${tradeData.requestedPrice} to ${tradeData.type} has been submitted`);
    this.onFetchTrades();

  }

  onFetchTrades() {
    // Send Http request
    this.isFetching = true;
    this.tradesService.fetchTrades().subscribe(
      trades => {
        this.isFetching = false;
        this.loadedTrades = trades;
      },
      error => {
        this.error = error.message;
        console.log(error);
      }
    );
  }

  onFetchStocks() {
    // Send Http request
    this.isFetching = true;
    //this.onFetchLivePrices();
    this.tradesService.fetchStocks().subscribe(
      stocks => {
        this.isFetching = false;
        this.loadedStocks = stocks;
      },
      error => {
        this.error = error.message;
        console.log(error);
      }
    );
  }

  onFetchLivePrices(){
    for(let stock of this.loadedStocks){
      this.tradesService.getLivePrice(stock.tickerName).subscribe(
        price => {
          this.livePriceMap.set(stock.tickerName,price);
        },
        error => {
          this.error = error.message;
          console.log(error);
        }
      );
    }
  }

  onGetUserCashAmount(){
    this.tradesService.getUserCashAmount().subscribe(
      cashAmount => {
        this.userCashAmount = cashAmount.toFixed(2);
      },
      error => {
        this.error = error.message;
        console.log(error);
      }
    );
  }

  onClearTrades() {
    // Send Http request
    this.tradesService.deleteTrades().subscribe(() => {
      this.loadedTrades = [];
    });
  }

  isAccountPositive(stock: Stock){
    return ((stock.quantity * this.livePriceMap.get(stock.tickerName)) - (stock.quantity * stock.price)) > 0
  }

  /* async onGetLivePrice(ticker) {
    if(!ticker) {
      alert('👿 👿 👿 Please enter a ticker first. 👿 👿 👿');
      return;
    }

    // Make request
    var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
    const getPriceUrl = priceServiceUrl + '?ticker=' + ticker + '&num_days=1';

    const response = await fetch(getPriceUrl);
    const json = await response.json()
    if(!json['price_data']) {
      alert('👿 👿 👿 Invalid ticker! 👿 👿 👿');
      return;
    }

    var lastPrice = json['price_data'][0][1]

    (<HTMLInputElement>document.getElementById('requestedPrice')).value = lastPrice.toFixed(2);

    alert('Live price for ' + ticker + ' is: $ ' + lastPrice.toFixed(2) + ' and has been suggested. Please adjust/refill the price field before submitting.')

  } */

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }

}

