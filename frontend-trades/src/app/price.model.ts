export interface Price {
    ticker: string;
    date: string;
    price: number;
    price_data: string[][];
}