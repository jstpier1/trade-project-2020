import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';

import { Trade } from './trade.model';
import { Stock }  from './stock.model';
import { tick } from '@angular/core/testing';
import { Price } from './price.model';
import { User } from './user.model';


@Injectable({ providedIn: 'root' })
export class TradesService {
  error = new Subject<string>();
  livePriceEntry: number;

  constructor(private http: HttpClient) {}

  configUrl = 'http://localhost:8080/api/trades';
  stockUrl = 'http://localhost:8080/api/trades/stock';
  userUrl = 'http://localhost:8080/api/trades/myuser';
  liveUrl = 'http://localhost:5000/v1/price/';

  createAndStoreTrade(ticker: string, quantity: number, requestedPrice: number, type: string) {

      const tradeData: Trade = { ticker: ticker, quantity: quantity, requestedPrice: requestedPrice, type: type };
      this.http
        .post<{ name: string }>(
          this.configUrl,
          tradeData
        )
        .subscribe(
          responseData => {
          },
          error => {
            this.error.next(error.message);
            alert("👿 👿 👿 Invalid ticker \"" + ticker + "\"! Please try again. 👿 👿 👿");
          }
        );
  }

  fetchTrades() {
    return this.http
      .get<{ [key: string]: Trade }>(
        this.configUrl
      )
      .pipe(
        map(responseData => {
          const tradesArray: Trade[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {

              // Turned off live pricing for trade history for performance reasons
              /* async function submitQuery (ticker: string) {
                var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
                var reqUrl = priceServiceUrl + '?ticker=' + ticker + '&num_days=1';
                const response = await fetch(reqUrl);
                const json = await response.json();
                var price = json['price_data'][0][1];
                return price;
              }

              submitQuery(responseData[key]['ticker'])
                .then(val => {(responseData[key])['livePrice'] = val.toFixed(2);
                              tradesArray.push({ ...responseData[key], id: key })});
            }
          } */

              tradesArray.push({ ...responseData[key], id: key });
            }
          }
          return tradesArray;
        }),
        catchError(errorRes => {
          // Send to analytics server
          return throwError(errorRes);
        })
      );
  }

  fetchStocks() {
    return this.http
      .get<{ [key: string]: Stock }>(
        this.stockUrl
      )
      .pipe(
        map(responseData => {
          const stocksArray: Stock[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {

              // async function submitQuery (ticker: string) {
              //   var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
              //   var reqUrl = priceServiceUrl + '?ticker=' + ticker + '&num_days=1';
              //   const response = await fetch(reqUrl);
              //   const json = await response.json();
              //   var price = json['price_data'][0][1];
              //   return price;
              // }

              // submitQuery(responseData[key]['tickerName'])
              //   .then(val => {(responseData[key])['livePrice'] = val.toFixed(2);
                              stocksArray.push({ ...responseData[key], id: key });

            }
          }
          return stocksArray;
        }),
        catchError(errorRes => {
          // Send to analytics server
          return throwError(errorRes);
        })
      );
  }

  getLivePrice(ticker :string){
    //var tempUrl = this.liveUrl + ticker;
    var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
    var reqUrl = priceServiceUrl + '?ticker=' + ticker + '&num_days=1';
    return this.http
      .get<Price>(
        reqUrl
      )
      .pipe(
        map(responseData => {
          return responseData.price_data[0][1];
        }),
        catchError(errorRes => {
          // Send to analytics server
          return throwError(errorRes);
        })
      );

  }

  getUserCashAmount(){
    return this.http
      .get<User>(
        this.userUrl
      )
      .pipe(
        map(responseData => {
          return responseData.cashAmount;
        }),
        catchError(errorRes => {
          // Send to analytics server
          return throwError(errorRes);
        })
      );

  }

  deleteTrades() {
    return this.http.delete(
      this.configUrl
    );
  }
}







