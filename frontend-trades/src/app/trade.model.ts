export interface Trade {
    id?: string;
    dateCreated?: string;
    ticker: string;
    quantity: number;
    requestedPrice: number;
    tradeState?: string;
    //livePrice?: number;
    type: string;
    price?: number;
}
