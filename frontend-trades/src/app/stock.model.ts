export interface Stock {
    id?: string;
    name: string;
    tickerName: string;
    quantity: number;
    price: number;
    livePrice?: number;
}
