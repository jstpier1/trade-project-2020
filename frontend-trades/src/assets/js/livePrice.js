var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed'

async function getLivePrice(ticker, numDays) {
  if(typeof(numDays) == 'undefined') {
      numDays = 1;
  }

  const response = await fetch(priceServiceUrl + '?ticker=' + ticker + '&num_days=' + numDays);
  const json = await response.json()
  return json;
}

async function getMostRecentClosePrice(ticker) {
  //console.log(ticker + ' ' + (await getLivePrice(ticker, 1))['price_data'][0][1]);
  return (await getLivePrice(ticker, 1))['price_data'][0][1];
}

function getLastClosePriceExt(ticker) {
  var priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
  var reqUrl = priceServiceUrl + '?ticker=' + ticker + '&num_days=1';

  fetch(reqUrl).then(data => {
                    console.log(data.json());
                    document.getElementById('pricePlace').value = data.json()['price_data'][0][1];
                    return ((data.json())['price_data'][0][1]).toString()})
                .then(res => console.log(res))
                .catch(error => console.log(error));

}


