function clearViz() {
  document.getElementById('researchTicker').value='';
  document.getElementById('lblGreetings').innerHTML = ""
  document.getElementById('totalPositionValue').innerHTML = "";
  document.getElementById('positionDeltaValue').innerHTML = "";
  document.getElementById("totalNumberOfInvestments").innerHTML = "<small><b>Investments</b></small>";
  document.getElementById("totalNumberOfPositions").innerHTML = "<small><b>Positions</b></small>";
  document.getElementById("availableCash").innerHTML = "<small><b>Available</b></small>";
}

// Generate portfolio visualization
function loadViz(numDays) {
  document.getElementById('researchTicker').value='';

  loadStocks(numDays, false);
}

async function loadStocks(numDays) {
  const stockUrl = 'http://localhost:8080/api/trades/stock';
  const response = await fetch(stockUrl);
  await response.json().then(res => prepareStocksRequests(res, numDays));
}

function prepareStocksRequests(json, numDays) {

  if(typeof(numDays) == 'undefined') {
    numDays = 1;
  }

  const urls = [];
  const quantities = [];
  const priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';

  for (var key in json) {
    if (json.hasOwnProperty(key)) {
      const url = priceServiceUrl + '?ticker=' + json[key]['tickerName'] + '&num_days=' + numDays;
      urls.push(url);
      quantities.push(json[key]['quantity']);
    }
  }

  makeRequestsAndSumDailyPrices(urls, quantities);
}


function makeRequestsAndSumDailyPrices (urls, quantities) {
  const price_data_agg = {};
  const price_data_agg_list = [];

  const promises = [];
  for (let i = 0; i < urls.length; i++) {
    promises.push(d3.json(urls[i]).then(res => {
      for(var j in res['price_data']) {
        res['price_data'][j].push(quantities[i]);
      }
      return res;
    }));
  }

  Promise.all(promises).then(data => {

    var numberOfInvestments = data.length;
    var numberOfPositions = 0;

    for(let i = 0; i < data.length; i++) {
      var priceDataEntry = data[i]['price_data'];

      for(let j = 0; j < priceDataEntry.length; j++) {
        var date = priceDataEntry[j][0].toString();
        var price = priceDataEntry[j][1];
        var quantity = priceDataEntry[j][2];

        numberOfPositions += quantity;

        if(!(date in price_data_agg)) {
          price_data_agg[date] = price * quantity;
        } else {
          price_data_agg[date] += price * quantity;
        }
      }
    }

    for (var key in price_data_agg) {
      price_data_agg_list.push({"date": key, "value" : price_data_agg[key]});
    }

    generateAndShowViz(price_data_agg_list, "Entire Portfolio");
    showGreeting();
    showTodayPortfolioValue(price_data_agg_list.slice(price_data_agg_list.length - 2, price_data_agg_list.length));
    showPositionsShares(numberOfInvestments, numberOfPositions);
    showCashBalance();
  });
}

function generateAndShowViz(data, companyName) {
  if(data.length == 0) {
    console.log("Empty agg price data!");
    return;
  }

  d3.select("#portfolioContainer").selectAll("svg").remove();

  // Set the dimensions of the canvas / graph
  var margin = {top: 50, right: 20, bottom: 30, left: 50},
  width = 800 - margin.left - margin.right,
  height = 400 - margin.top - margin.bottom;

  // Parse the date / time
  var parseDate = d3.timeParse("%Y-%m-%d");

  // Set the ranges
  var x = d3.scaleTime().range([0, width]);
  var y = d3.scaleLinear().range([height, 0]);

  // Define the axes
  var xAxis = d3.axisBottom(x);
  var yAxis = d3.axisLeft(y);

  // Define the line
  var valueline = d3.line()
  .x(function(d) { return x(d.date); })
  .y(function(d) { return y(d.value); });

  // Adds the svg canvas
  var svg = d3.select("#portfolioContainer")
  .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
  .append("g")
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

  data.forEach(function(d) {
      d.date = parseDate(d.date);
      d.value = +d.value;
  });

  // Scale the range of the data
  x.domain(d3.extent(data, function(d) { return d.date; }));
  const min = d3.min(data, d => d.value);
  const max = d3.max(data, d => d.value);
  y.domain([min - 0.05*min, max + 0.05*max]);

  // Add the valueline path.
  var path = svg.append("path")
      .attr("class", "line")
      .attr("d", valueline(data));

  // Variable to Hold Total Length
  var totalLength = path.node().getTotalLength();

  // Set Properties of Dash Array and Dash Offset and initiate Transition
  path
    .attr("stroke-dasharray", totalLength + " " + totalLength)
    .attr("stroke-dashoffset", totalLength)
    .transition() // Call Transition Method
    .duration(1000) // Set Duration timing (ms)
    .ease(d3.easeLinear) // Set Easing option
    .attr("stroke-dashoffset", 0); // Set final value of dash-offset for transition

  // Add the X Axis
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  // Add the Y Axis
  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  // Add title
  svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 8))
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "bold")
        .style("fill", "white")
        .text("Performance for " + companyName);

}

function showGreeting() {
  if(document.getElementById('lblGreetings').innerHTML === "") {
    var myDate = new Date();
    var hrs = myDate.getHours();

    var greet;

    if (hrs < 12)
        greet = 'Good Morning';
    else if (hrs >= 12 && hrs <= 17)
        greet = 'Good Afternoon';
    else if (hrs >= 17 && hrs <= 24)
        greet = 'Good Evening';

    document.getElementById('lblGreetings').innerHTML ='<b>' + greet + '!</b>';
  }
}

function showTodayPortfolioValue(data) {

  if(document.getElementById("totalPositionValue").innerHTML === "") {
    var date = (new Date()).toISOString().split('T')[0];
    var todayValue = data[1]['value'].toFixed(2);
    var yesterdayValue = data[0]['value'].toFixed(2);
    var delta = (todayValue - yesterdayValue).toFixed(2);

    document.getElementById("totalPositionValue").innerHTML = "$ " + todayValue;
    if(delta < 0) {
      document.getElementById("positionDeltaValue").innerHTML = "<i>Down $ " + delta*-1 + " from yesterday</i>";
      document.getElementById("positionDeltaValue").setAttribute("class", "text-danger");
    } else if (delta > 0) {
      document.getElementById("positionDeltaValue").innerHTML = "<i>Up $ " + delta + " from yesterday</i>";
      document.getElementById("positionDeltaValue").setAttribute("class", "text-success");
    } else {
      document.getElementById("positionDeltaValue").innerHTML = "<i>Same as yesterday</i>";
      document.getElementById("positionDeltaValue").setAttribute("class", "text-muted");
    }
    document.getElementById("dateToRender").innerHTML += ", " + date;
  }


}

function showPositionsShares(numberOfInvestments, numberOfPositions) {
  if(document.getElementById("totalNumberOfInvestments").innerHTML === "<small><b>Investments</b></small>") {
    document.getElementById("totalNumberOfInvestments").insertAdjacentHTML("afterbegin", numberOfInvestments + " ");
    document.getElementById("totalNumberOfPositions").insertAdjacentHTML("afterbegin", numberOfPositions + " ");
  }
}

async function showCashBalance() {
  if(document.getElementById("availableCash").innerHTML === "<small><b>Available</b></small>") {
    userUrl = 'http://localhost:8080/api/trades/myuser';

    const response = await fetch(userUrl);
    const json = await response.json();
    document.getElementById("availableCash").insertAdjacentHTML("afterBegin", "$" + json['cashAmount'].toFixed(2) + " ");

  }
}


// Generate viz for researching a ticker (borrows function above)
function researchTicker(ticker, numDays) {
  if(!ticker) {
    alert("Please enter a ticker, or select a Portfolio option.")
    return;
  }

  if(typeof(numDays) == 'undefined') {
    numDays = 1;
  }

  const priceServiceUrl = 'https://qxup5m1v5f.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed';
  const url = priceServiceUrl + '?ticker=' + ticker + '&num_days=' + numDays;

  d3.json(url).then(res => {
    const priceData = res['price_data'];
    const ticker = res['ticker'];
    parseResearchPrices(priceData, ticker);
  })
  .catch(err => alert("Invalid ticker, please try again."));
}

function parseResearchPrices(json, ticker) {
  const data = [];
  for (var key in json) {
    const date = json[key][0].toString();
    const value = json[key][1];
    data.push({"date": date, "value" : value});
  }

  generateAndShowViz(data, ticker);
}

// Load stocks & news clips for those stocks
async function loadStocksForNews() {
  document.getElementById("newsContainer").innerHTML += "<ul class='list-group'>";
  const stockUrl = 'http://localhost:8080/api/trades/stock';
  const response = await fetch(stockUrl);
  await response.json().then(json => {
    tickers = [];
    for (var key in json) {
      if (json.hasOwnProperty(key)) {
        tickers.push(json[key]['tickerName']);
      }
    }

    for (var i in tickers) {
      pullCompanyFromTicker(tickers[i]);
    }

    document.getElementById("newsContainer").innerHTML += "</ul>";
  });
}

async function pullCompanyFromTicker(ticker) {
  const companiesURL = "https://financialmodelingprep.com/api/v3/search?query=" + ticker + "&limit=1&apikey=demo";
  const response = await fetch(companiesURL);
  await response.json().then(json => {
    var companyNameString = json[0]['name'];
    companyNameString = companyNameString.substring(0, companyNameString.indexOf(" "));
    if(companyNameString.includes(".")) {
      companyNameString = companyNameString.substring(0, companyNameString.indexOf("."));
    }
    pullNewsForCompany(ticker, companyNameString);
  })

}

async function pullNewsForCompany(ticker, company) {

  // News API: https://newsapi.org/
  const newsapiKey = "cf572c575d334980a1b39720d724b90d";
  const today = (new Date()).toISOString().split('T')[0];
  var maxArticlesPerCompany = 5;

  const proxyUrl = "https://cors-anywhere.herokuapp.com/";
  const newsUrl = "http://newsapi.org/v2/top-headlines?q=" + company + "&from=" + today + "&to=" + today + "&sortBy=popularity&apiKey=" + newsapiKey;
  //const response = await fetch(proxyUrl + newsUrl);
  const response = await fetch(newsUrl);
  await response.json().then(json => {

    var articlesList = json['articles'];

    if(articlesList.length == 0) {
      return;
    }

    if(articlesList.length > maxArticlesPerCompany) {
      renderNewsForCompany(ticker, articlesList.slice(0, maxArticlesPerCompany));
    } else {
      renderNewsForCompany(ticker, articlesList);
    }
  }).catch(
    () => console.log("CORS error!")
  );
}

function renderNewsForCompany(ticker, articlesList) {
  var newsBox = document.getElementById("newsContainer");

  var newsHTMLString = ""
  for (var i in articlesList) {
    var article = articlesList[i];
    newsHTMLString += "<li class='list-group-item small'>";
    newsHTMLString += "<b>" + ticker + "</b> &nbsp; &nbsp; &nbsp;";
    newsHTMLString += "<a href='" + article['url'] + "'>";
    newsHTMLString += "<i>" + article['source']['name'] + "</i>";
    newsHTMLString += "</a> &nbsp; &nbsp; &nbsp;";
    newsHTMLString += (article['title']) + " &nbsp; &nbsp; &nbsp;";
    newsHTMLString += "<div class='text-muted small'>" + (article['content']) + "</div>";
    newsHTMLString += "</li>";
  }

  newsBox.innerHTML += newsHTMLString;
}



